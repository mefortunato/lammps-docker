FROM debian:stretch

ARG PACKAGES="molecule class2 kspace user-misc misc"

RUN apt-get update && \
    apt-get install -y python python-dev python-pip make git g++ libopenmpi-dev openmpi-bin && \
    rm -rf /var/lib/apt/lists/*

RUN git clone https://github.com/lammps/lammps.git && \
    cd lammps/src && \
    for PACKAGE in $PACKAGES; do make yes-$PACKAGE; done && \
    make -j2 mpi && \
    cp lmp_mpi /usr/local/bin && \
    cd ../../ && \
    rm -rf lammps
